#![feature(generators, generator_trait)]
use std::env;

use std::ops::{Generator, GeneratorState};

fn prime_factors(arg: u64) -> String {
    let mut primes = || {
        let mut cur_primes: Vec<u64> = Vec::new();
        let mut cur_num = 1;
        'loop_primes: loop {
            cur_num += 1;
            'checking_primes: for prime in &cur_primes {
                if cur_num % prime == 0 {
                    continue 'loop_primes;
                }
                if prime.pow(2) > cur_num {
                    break 'checking_primes;
                }
            }
            cur_primes.push(cur_num);
            yield cur_num;
        }
    };

    let mut factors: Vec<String> = Vec::new();
    let mut rest = arg;
    while rest > 1 {
        let prime = match primes.resume() {
            GeneratorState::Yielded(prime) => prime,
            _ => panic!("should not happen")
        };
        let mut multiplicity = 0;
        while rest > 0 && rest % prime == 0 {
            rest /= prime;
            multiplicity += 1;
        }

        if multiplicity == 1 {
            factors.push(format!("{}", prime));
        } else if multiplicity > 1 {
            factors.push(format!("{}^{}", prime, multiplicity));
        } else if prime.pow(2) > rest {
            factors.push(format!("{}", rest));
            break;
        }
    }

    factors.join(" x ")
}

fn main() {
    let argument_as_str = env::args().nth(1)
        .expect("expected one argument");
    let argument:u64 = argument_as_str.parse().expect("argument should be a number");

    println!("réponse = {}", prime_factors(argument));

}

#[test]
fn it_should_compute_prime_factors_for_prime_numbers() {
    assert_eq!(prime_factors(17), "17");
    assert_eq!(prime_factors(5), "5");
}

#[test]
fn it_should_compute_prime_factors_for_non_prime_numbers() {
    assert_eq!(prime_factors(6), "2x3");
    assert_eq!(prime_factors(12), "2^2x3");
    assert_eq!(prime_factors(9), "3^2");
    assert_eq!(prime_factors(45), "3^2x5");
    assert_eq!(prime_factors(360), "2^3x3^2x5");
}
